const redis = require('redis');
const bluebird = require('bluebird')
bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);
const redisModel = require("../redisModel");
const model = require('../model')
const successResponse = require('../misc/successResponse');
const mailHeper = require('../helper/sendMail');
const constant = require('../constant');
exports.ingestEvent = async (req, res) => {
    let body = req.body;
    try {
        var dataIngest = await model.event.ingestEvent(body);
        let redisStats = await checkRedisKeys(dataIngest, req.user);
        redisStats.hourlyTransactionAmount > parseInt(constant.TotalHighAmountDebit) && body.data.data.action == "debit" ?  sendHighTransactionAmountTotalMail(redisStats) : false;
        redisStats.daysTransactionCount > parseInt(constant.HighFrequentTransactionCount)?  sendFrequentTransactionPerDayMail(redisStats) : false;
    } catch (e) {
        console.log(e)
        res.status(500).send({ error: 'Something went wrong!Please try again' });
    }
    res.send(successResponse('dataIngest'));

}

const checkRedisKeys = async (dataIngest, user) => {
    let userKey = `${user['username']}`;
    let userData = {
        'username': user['username'],
        'email': user['email']
    }
    let val = await redisModel.redisClient.hmsetAsync(userKey, userData);
    if (await redisModel.redisClient.existsAsync(`${userKey}@hourlyTransaction`)) {
        await redisModel.redisClient.incrbyAsync(`${userKey}@hourlyTransaction`, dataIngest.data.data.amount)
    } else {
        await redisModel.redisClient.setexAsync(`${userKey}@hourlyTransaction`, 3600, dataIngest.data.data.amount)
    }

    if (await redisModel.redisClient.existsAsync(`${userKey}@dayTransactionCount`)) {
        await redisModel.redisClient.incrbyAsync(`${userKey}@dayTransactionCount`, 1)
    } else {
        await redisModel.redisClient.setexAsync(`${userKey}@dayTransactionCount`, 86400, 1)
    }
    userData['hourlyTransactionAmount'] = await redisModel.redisClient.getAsync(`${userKey}@hourlyTransaction`)
    userData['daysTransactionCount'] = await redisModel.redisClient.getAsync(`${userKey}@dayTransactionCount`);
    return userData;

}

const sendHighTransactionAmountTotalMail = async (userData) => {
     mailHeper.sendMail(userData,constant.TotalHighAmountDebitBody);
}
const sendFrequentTransactionPerDayMail = async () => {
     mailHeper.sendMail(userData,constant.HighFrequentTransactionCountBody);
}

