const mongoose = require('mongoose');
const schema = mongoose.Schema;

module.exports = new schema({
    userId: schema.ObjectId,
    username : String,
    password: String,
    phoneNumber:Number,
    email:String
})