const nodemailer = require('nodemailer');
const config = require('../config');

exports.sendMail = async (userData, messagebody) => {
    return new Promise(function (resolve, reject) {
        nodemailer.createTestAccount((err, account) => {
            var transporter = nodemailer.createTransport({
                host: 'smtp.ethereal.email',
                port: 587,
                secure: false,
                auth: {
                    user: config.get('gmail.user'),
                    pass: config.get('gmail.password')
                }
            });
            let mailOptions = {
                from: config.get('gmail.user'),
                to: userData['email'],
                subject: 'Transaction Alert',
                text: messagebody,
                html: '<b>Hello world?</b>'
            };
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log(error);
                    reject(error)
                }
                console.log(info);
                resolve();
            });
        });
    });
}
