const fs = require("fs");
const path = require("path");
let model = {};
const config = require('../config');
//Read all models files and create a model Object.

fs
    .readdirSync(__dirname)
    .filter(file => {
        return (file.indexOf(".") !== 0) && (file !== "index.js");
    })
    .forEach(file => {
        let file_name = file.replace(".js", "");
        model[file_name] = require(path.join(__dirname, file));
    });
module.exports = model;