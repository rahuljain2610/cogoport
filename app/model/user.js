const mongoose = require('mongoose')
const schema = require('../schema');
const model = require('../model');
const userModel = mongoose.model('users',schema.user);


userModel.getUserInfoByUsernamePassword = async(username,password)=>{
    return new Promise((resolve, reject) => {
        userModel.findOne({ "username": username,"password":password }, (err, result) => {
            if (err) {
                reject();
            }
            if (result) {
               
                resolve(result);
            }else{
                resolve(result)
            }
        })
    })

}


userModel.checkIfUsernameExists = async (username) => {
    return new Promise((resolve, reject) => {
        userModel.findOne({ "username": username }, (err, result) => {
            if (err) {
                
                reject();
            }
            if (result) {
                resolve(true);
            } else {
                resolve(false);
            }
        })
    })

}




module.exports = userModel;