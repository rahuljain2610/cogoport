const mongoose = require('mongoose')
const schema = require('../schema');
const model = require('../model');
const eventModel = mongoose.model('event', schema.event);

eventModel.ingestEvent = (body)=>{
    return new Promise(function(resolve,reject){
        let eventInstance = new eventModel(body);
        eventInstance.save((err, event) => {
        if (err) {
            reject(err);
         }
            resolve(event);
        });
    }
    )}


module.exports = eventModel;