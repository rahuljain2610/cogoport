const fs = require("fs");
const path = require("path");
let model = {};
const config = require('../config')
const mongoose = require('mongoose');
try{
    mongoose.connect(`mongodb://${encodeURIComponent(config.get('database_mongodb.username'))}:${encodeURIComponent(config.get('database_mongodb.password'))}@${config.get('database_mongodb.host')}:${config.get('database_mongodb.port')}/${config.get(`database_mongodb.dbName`)}`)

}catch(e){
   
    res.status(500).send({ error: 'Connection Failed,Please try again.' })
}

//Read all models files and create a model Object.

fs
    .readdirSync(__dirname)
    .filter(file => {
        return (file.indexOf(".") !== 0) && (file !== "index.js");
    })
    .forEach(file => {
        let file_name = file.replace(".js", "");
        model[file_name] = require(path.join(__dirname, file));
    });

module.exports = model;