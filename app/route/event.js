const express = require('express');
const router = express.Router();
const controller = require(`../controller`);
const authenticateUser = require('../middleware/authenticate');
router.post('/',authenticateUser.authenticate,controller.event.ingestEvent);
module.exports = router;