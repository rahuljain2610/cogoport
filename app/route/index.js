"use strict"
const fs = require("fs");
const path = require("path");
const express = require('express');
// const app = new express();
const router = express.Router();
const user = require('./user');

const event = require('./event');
router.use('/user',user);
router.use('/event',event);
module.exports = router;
