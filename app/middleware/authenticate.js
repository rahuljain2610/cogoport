const jwt = require('jsonwebtoken');
const successResponse = require('../misc/successResponse');
exports.authenticate = async(req,res,next)=>{
    
    if(!req || !req.headers || !req.headers.authorization){
      res.status(401).send({ error: 'Unauthorized Request!' })
    }
    try{
      var userInfo = jwt.verify(req.headers.authorization,"secret");
    }catch(e){
      res.status(401).send({ error: 'Unauthorized Request Token!' })
    }
   
    
    req.user = userInfo;
    next();
}