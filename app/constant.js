function define(name, value) {
    Object.defineProperty(exports, name, {
        value: value,
        enumerable: true
    });
}

// Message for internal server errors in production
define("TotalHighAmountDebit", "10000");
define("HighFrequentTransactionCount", "10");
define("TotalHighAmountDebitBody", "Hi Your total amount debited in last one hour is more than 10000");
define("HighFrequentTransactionCountBody", "'Hi Your total transactions are more than 10 in last 24 hours'");
