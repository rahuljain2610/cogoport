
git clone https://rahuljain2610@bitbucket.org/rahuljain2610/cogoport.git

npm install

edit gmail user and password for email services



## API Collection And Details

- Postman Collection Link : https://www.getpostman.com/collections/d5579e9309c0c48545ea

- Postman Production Environment: Development.postman_environment.json

## Other Notes

- These APIs are deployed on heroku

- heroku URL - https://eventspub.herokuapp.com/

- Extra /login API is created for JWT integration User registeration and login

- Mongodb is hosted on mlabs. (https://mlab.com/)
- Redis is hosted on redislabs 